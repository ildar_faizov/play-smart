import React, { Component } from 'react';
import { Container, Row, Col, Alert } from 'reactstrap';
import { Loading } from './LoadingComponent';
import './GameComponent.css';

export default class Game extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isGameInitialized: false,
            api: null,
            timerElapsed: false
        };

        this.canvasRef = React.createRef();
        this.timerId = null;
    }

    componentDidMount() {
        const id = this.props.match.params.id;
        console.log(`Game with id ${id} started`);
        this.props.fetchGame(id);
    }

    componentDidUpdate(prevState) {
        if (this.timerId == null && this.props.timer.startTime != null && this.props.timer.durationInSeconds != null) {
            // timer is set
            this.timerId = setInterval(() => this.verifyTimeout(), 1000);
        } else if (this.timerId != null && (this.props.timer.startTime == null || this.props.timer.durationInSeconds == null)) {
            // running timer is cancelled
            clearInterval(this.timerId);
            delete this.timerId;
            if (prevState.timerElapsed) {
                this.setState({timerElapsed: false});
            }
        }

        // game init
        if (this.props.game.game && !this.state.isGameInitialized && this.canvasRef.current != null) {
            const game = this.props.game.game;
            if (game.instance.init) {
                const api = this.createApi();
                game.instance.init(this.canvasRef.current, api);
                this.setState({
                    isGameInitialized: true,
                    api
                });
            }
        }

        // timer elapsed
        if (!prevState.timerElapsed && this.state.timerElapsed) {
            if (this.props.game.game && this.state.isGameInitialized) {
                this.props.game.game.instance.destroy();
                this.setState({
                    isGameInitialized: false            
                });
            }
        }
    }

    componentWillUnmount() {
        if (this.props.game.game && this.state.isGameInitialized) {
            const game = this.props.game.game;
            if (game.instance.destroy) {
                game.instance.destroy();
            }
        }

        if (this.timerId != null) {
            clearInterval(this.timerId);
            delete this.timerId;
        }
    }

    render() {
        if (this.props.game.isLoading) {
            return (
                <Container>
                    <Loading/>
                </Container>
            );   
        } else if (this.props.game.error) {
            return (
                <Container>                
                    <p className="text-danger">{this.props.game.error}</p>
                </Container>
            );
        } else if (this.state.timerElapsed) {
            return (
                <Container>
                    <Alert color="warning">Sorry, it's time to have some rest</Alert>
                </Container>
            );
        } else if (this.props.game.game) {
            const game = this.props.game.game;
            return (
                <Container>
                    <header className="row align-items-baseline">
                        <Col xs="12" sm="9">
                            <h1>{game.title}</h1>        
                        </Col>
                        <Col xs="auto" sm="3">
                            <p className="font-italic">Version {game.version} by {game.creator}</p>
                        </Col>
                    </header>
                    <Row>
                        <canvas ref={this.canvasRef} className="col-12 game-canvas"></canvas>
                    </Row>
                </Container>
            );
        }
    }

    createApi() {
        return {
            game: this.props.game.game
        };
    }

    verifyTimeout() {
        this.setState((prevState, props) => {
            const timerElapsed = props.timer.startTime.getTime() - new Date().getTime() 
                + props.timer.durationInSeconds * 1000 <= 0;
            return prevState.timerElapsed !== timerElapsed ? {timerElapsed} : {};
        });
    }
}