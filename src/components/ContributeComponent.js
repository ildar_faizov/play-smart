import React, { Component } from 'react';
import { Container, Row, Col, FormGroup, Label, Button } from 'reactstrap';
import { Panel } from './PanelComponent';
import { LocalForm, Control, Errors } from 'react-redux-form';
import CommonValidators from '../shared/commonFormValidators';
import { MIN_AGE, MAX_AGE } from '../redux/filterForm';
import _ from 'lodash';
import { Base64 } from 'js-base64';

export const Contribute = (props) => {
  return (
    <Container>
      <Row>
        <Col xs="12">
          <UploadGame
            fetchAreas={props.fetchAreas}
            areas={props.areas}
            uploadGame={props.uploadGame}/>
        </Col>
      </Row>
      <Row>
        <Col xs="12">
          <ManageGames/>
        </Col>
      </Row>
    </Container>
  );
}

class UploadGame extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchAreas();
  }

  render() {
    let areaCheckboxes;
    if (this.props.areas.areas != null) {    
      areaCheckboxes = this.props.areas.areas.map(area => {
        const id = `contribute.area-${area.id}`;
        return (
          <div className="form-check form-check-inline" key={area.id}>
            <Control.checkbox
              id={id}
              model="form.areas[]"
              value={area.id}
              className="form-check-input"
            />
            <Label htmlFor={id} className="form-check-label">{area.title}</Label>
          </div>
        );
      });
    } else {
      areaCheckboxes = (
        <FormGroup>
          <span className="fa fa-spinner fa-lg"/>
        </FormGroup>
      );
    }
    return (
      <Panel title="Game Upload" className="mb-4">
        <LocalForm model="form" 
          onSubmit={this.handleSubmit}
          validators={{
            '': {
              areasSpecified: (vals) => UploadGame.areasSpecified(vals)
            }
          }}>
          <FormGroup row>
            <Label htmlFor="contribute.title" sm="2" xs="12" className="col-form-label">Title</Label>
            <Col sm="10" xs="12">
              <Control.text 
                model=".title"
                className="form-control"
                id="contribute.title"               
                placeholder="Anaconda"
                validators={{
                  required: CommonValidators.required,
                  correct: CommonValidators.boundedString({minLength: 3, maxLength: 100})
                }}/>
              <Errors model=".title" className="text-danger" show="touched"
                messages={{
                  required: 'Required',
                  correct: 'Must be between 3 and 100 characters'
                }}/>
            </Col>            
          </FormGroup>
          <FormGroup row>
            <Label htmlFor="contribute.version" sm="2" xs="12" className="col-form-label">Version</Label>
            <Col sm="10" xs="12">
              <Control.text 
                model=".version"
                className="form-control" sm="10" xs="12"
                id="contribute.version" 
                placeholder="1.0"
                validators={{
                  required: CommonValidators.required,
                  correct: CommonValidators.byRegExp(/^\d+(\.\d+)?$/)
                }}/>
              <Errors model=".version" className="text-danger" show="touched"
                messages={{
                  required: 'Required',
                  correct: 'Must be either a number or two numbers separated by dot, e.g. "1", "0.1", "12.23"'
                }} />
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label htmlFor="contribute.age.min" sm="2" xs="12" className="col-form-label">Age</Label>
            <Col sm="2" xs="5">
              <Control.text 
                model=".age.min"
                type="number"
                parser={parseInt}
                className="form-control" 
                id="contribute.version" 
                placeholder={MIN_AGE}
                validators={{
                  required: CommonValidators.required,
                  correct: CommonValidators.boundedInt({min: MIN_AGE, max: MAX_AGE})
                }}/>
              <Errors model=".age.min" className="text-danger" show="touched"
                messages={{
                  required: 'Required',
                  correct: `Must be a number between ${MIN_AGE} and ${MAX_AGE}`
                }}/>
            </Col>            
            <Label htmlFor="contribute.age.max" xs="auto" className="col-form-label">-</Label>
            <Col sm="2" xs="5">
              <Control.text 
                model=".age.max"
                type="number"
                parser={parseInt}
                className="form-control"
                id="contribute.version" 
                placeholder={MAX_AGE}
                validators={{
                  required: CommonValidators.required,
                  correct: CommonValidators.boundedInt({min: MIN_AGE, max: MAX_AGE})
                }}/>
              <Errors model=".age.max" className="text-danger" show="touched"
                messages={{
                  required: 'Required',
                  correct: `Must be a number between ${MIN_AGE} and ${MAX_AGE}`
                }}/>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label sm="2" xs="12" className="col-form-label">Areas</Label>
            <Col sm="10" xs="12">
              {areaCheckboxes}
              <Errors model="form" className="text-danger"
                messages={{
                  areasSpecified: 'At least one area must be selected'
                }}/>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label sm="2" xs="12" className="col-form-label">Logo</Label>
            <Col sm="10" xs="12">
              <Control.file model=".logo" accept=".jpg, .jpeg, .png, .svg" 
                validators={{
                  required: CommonValidators.required
                }}/>
              <Errors model=".logo" className="text-danger" show="touched"
                messages={{
                  required: 'Required'
                }}/>
            </Col>
          </FormGroup>
          <FormGroup row>
            <Label sm="2" xs="12" className="col-form-label">Source Code</Label>
            <Col sm="10" xs="12">
              <Control.file model=".source" accept=".js"
                validators={{
                  required: CommonValidators.required
                }}/>
              <Errors model=".source" className="text-danger" show="touched"
                messages={{
                  required: 'Required'
                }}/>
            </Col>
          </FormGroup>

          <Button color="primary" type="submit">Upload Game</Button>
        </LocalForm>
      </Panel>
    );
  }

  handleSubmit(values) {
    const game = _.cloneDeep(values);
    delete game.logo;
    delete game.source;
    const promise = Promise.all([
      UploadGame.fileToBase64(values.logo), 
      UploadGame.fileToBase64(values.source)
    ]);
    promise.then(([logo, source]) => {
      game.logo64 = logo;
      game.source64 = source;
      this.props.uploadGame(game);
    });
  }

  static fileToBase64(files) {
    const file = files[0];
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.onload = (e) => resolve(Base64.encode(e.target.result));
      reader.onerror = (error) => reject(error);
      reader.readAsBinaryString(file);
    });
  }

  static areasSpecified(values) {
    return values.areas && values.areas.length > 0;
  }
}

function ManageGames(props) {
  return (
    <Panel title="Manage Your Games">
      Table with games
    </Panel>
  );
}