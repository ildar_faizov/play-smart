import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Switch, Route, Redirect, withRouter } from 'react-router-dom';
import Home from './HomeComponent';
import { fetchAreas, fetchGames, fetchGame, tryLogin, logout, setTimer, uploadGame } from '../redux/ActionCreators';
import Header from './HeaderComponent';
import { Contribute } from './ContributeComponent';
import { Parents } from './ParentsComponent';
import { Contact } from './ContactComponent';
import Game from './GameComponent';

const mapStateToProps = state => {
  return {
    areas: state.areas,
    filter: state.filter,
    games: state.games,
    account: state.account,
    timer: state.timer,
    currentGame: state.game,
    gameUpload: state.gameUpload
  }
};

const mapDispatchToProps = dispatch => ({
  fetchAreas: () => { dispatch(fetchAreas()) },
  fetchGames: (filter, pagination) => { dispatch(fetchGames(filter, pagination)) },
  fetchGame: (id) => { dispatch(fetchGame(id)) },
  tryLogin: (credentials) => { dispatch(tryLogin(credentials)) },
  logout: () => { dispatch(logout()) },
  setTimer: (duration) => { dispatch(setTimer(duration)) },
  uploadGame: (game) => { dispatch(uploadGame(game)) }
});

class Main extends Component {
  
  render() {
    console.log('MainComponent rendered');

    const renderHomePage = () => {
      return (
        <Home 
          fetchAreas={this.props.fetchAreas} 
          areas={this.props.areas} 
          filter={this.props.filter}
          games={this.props.games} 
          fetchGames={this.props.fetchGames}      
        />
      );
    };

    const renderGamePage = ({match}) => {
      return (
        <Game 
          fetchGame={this.props.fetchGame} 
          game={this.props.currentGame} 
          match={match}
          timer={this.props.timer}/>
      );
    }

    const renderContributePage = () => {
      return (
        <Contribute
          fetchAreas={this.props.fetchAreas}
          areas={this.props.areas}
          gameUpload={this.props.gameUpload}
          uploadGame={this.props.uploadGame}/>
      );
    }

    return (
      <React.Fragment>
        <Header 
          tryLogin={this.props.tryLogin} 
          account={this.props.account} 
          logout={this.props.logout}
          timer={this.props.timer}
          setTimer={this.props.setTimer}/>
        <Switch>
          <Route path="/home" render={ renderHomePage }/>
          <Route path="/games/:id" exact render={ renderGamePage }/>
          <Route path="/contribute" exact render={ renderContributePage }/>
          <Route path="/parents" exact component={ Parents }/>
          <Route path="/contacts" exact component={ Contact }/>
          <Redirect to="/home" />
        </Switch>
      </React.Fragment>
    );
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(Main));