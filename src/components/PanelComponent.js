import React from 'react';
import { Card, CardTitle, CardBody, Navbar, NavbarBrand } from 'reactstrap';

export function Panel(props) {
  return (
    <Card className={props.className}>
      <CardTitle className="text-white bg-info">
        <Navbar>
          <NavbarBrand>{props.title}</NavbarBrand>
          {props.titleComponent}
        </Navbar>
      </CardTitle>
      <CardBody>
        {props.children}
      </CardBody>
    </Card>
  );
};