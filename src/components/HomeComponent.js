import React, { Component } from 'react';
import { Container, Row, Col, Label, Button, Media, Badge } from 'reactstrap';
import { Control, Form } from 'react-redux-form';
import { Panel } from './PanelComponent';
import { Loading } from './LoadingComponent';
import _ from 'lodash';
import { BASE_URL } from '../shared/baseUrl';
import './HomeComponent.css';
import Paginator from './PaginatorComponent';
import { Link } from 'react-router-dom';

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.props.fetchAreas();
  }

  render() {
    return (
      <Form model="filter" onSubmit={this.handleSubmit}>
        <Container>
          <Row>
            <Col sm="12" md="4">
              <Filter fetchAreas={this.props.fetchAreas} areas={this.props.areas}/>
            </Col>
            <Col sm="12" md="8">
              <Results 
                games={this.props.games} 
                areas={this.props.areas} 
                filter={this.props.filter}
                fetchGames={this.props.fetchGames}/>
            </Col>
          </Row>
        </Container>
      </Form>
    );
  }

  handleSubmit(values) {
    const filter = {
      age: {
        min: parseInt(values.age.min, 10),
        max: parseInt(values.age.max, 10)
      },
      title: values.title,
      showVisited: values.showVisited
    };
    if (this.props.areas.areas) {
      filter.areas = _(this.props.areas.areas)
        .map(area => area.id)
        .filter(id => values[`area-${id}`] === true)
        .toArray();
    }
    
    const pagination = {
      pageLength: this.props.games.pageLength,
      currentPage: this.props.games.currentPage
    };

    this.props.fetchGames(filter, pagination);
  }
};

function RenderAreas(props) {
  if (props.areas.isLoading) {
    return (
      <Loading/>
    );
  } else if (props.areas.error) {
    return (
      <p className="text-danger">Areas couldn't be loaded from server: {props.areas.error}</p>
    );
  } else {
    return (props.areas.areas || []).map(area => {
      const name = `area-${area.id}`;
      return (
        <Row className="form-check" key={area.id}>
          <Control.checkbox 
            name={name} id={name} 
            model={'.' + name}
            className="form-check-input"/>
          <Label htmlFor={name}>{area.title}</Label>
        </Row>
      );
    });
  }
}

class Filter extends Component {
  render() {
    return (
      <Panel title="Filter">
        <Container>
          <Row className="form-group">
            <Label htmlFor="age_min" md="3" xs="12">Age</Label>
            <Col md="4" xs="6">
              <Control.text model=".age.min" id="age_min" name="age_min"
                className="form-control" type="number"/>
            </Col>
            <Label htmlFor="age_max" xs="0">-</Label>
            <Col md="4" xs="6">
              <Control.text model=".age.max" id="age_max" name="age_max"
                className="form-control" type="number"/>
            </Col>
          </Row>
          <Row className="form-group">
            <Label htmlFor="age_min" md="3">Areas</Label>
            <Col md="9">
              <RenderAreas areas={this.props.areas}/>
            </Col>
          </Row>
          <Row className="form-check">
            <Control.checkbox name="show_visited" id="show_visited"
              model=".showVisited"
              className="form-check-input"/>
            <Label htmlFor="show_visited">Show Visited</Label>
          </Row>
          <Button type="submit" color="primary">Search</Button>
        </Container>
      </Panel>
    );
  }
}

class Results extends Component {
  constructor(props) {
    super(props);

    this.toPage = this.toPage.bind(this);
  }

  render() {
    const SearchComponent = (
      <div className="form-inline">
        <Control.text name="search" model=".title" className="form-control"/>
      </div>
    );

    const title = (
      <React.Fragment>
        Results
        <Badge color="dark" className="ml-1" pill>{this.props.games.total}</Badge>
      </React.Fragment>
    );

    const numberOfPages = Math.ceil(this.props.games.total / this.props.games.pageLength);

    return (
      <Panel title={title} titleComponent={SearchComponent}>
        <RenderGames games={this.props.games}/>
        <Container>
          <Row>
            <Col xs="12">
              <Paginator 
                numberOfPages={numberOfPages}
                page={this.props.games.currentPage}
                setPage={this.toPage}
                centered
                />
            </Col>
          </Row>
        </Container>
      </Panel>
    );
  }

  toPage(page) {
    const filter = _.clone(this.props.filter);
    if (this.props.areas.areas) {
      filter.areas = [];
      _(this.props.areas.areas).map('id')
        .each(id => {
          const propName = `area-${id}`;
          if (filter[propName]) {
            delete filter[propName];
            filter.areas.push(id);
          }
        });      
    }

    const pagination = {
      pageLength: this.props.games.pageLength,
      currentPage: page
    };

    this.props.fetchGames(filter, pagination);
  }
}

function RenderGames(props) {
  if (props.games.isLoading) {
    return (<Loading/>);
  } else if (props.games.error) {
    return (
      <Container>
        <p className="text-danger">Error occured: {props.games.error}</p>
      </Container>
    );
  } else if (props.games.games.length === 0) {
    return (
      <Container>
        <p className="text-info">Nothing found</p>
      </Container>
    );
  } else {
    const games = props.games.games.map(game => <RenderGame game={game} key={game.id}/>);
    return (
      <Media list className="pl-0">
        {games}
      </Media>
    );
  }
}

function RenderGame(props) {
  const game = props.game;
  return (
    <Media className="p-2 mb-2">
      <Media top left>
        <Media object className="mr-3 game-logo" src={BASE_URL + 'sourcecode/' + game.logo} alt="Logo"/>
      </Media>
      <Media body>
        <Media heading className="w-100">
          <Link to={`/games/${game.id}`}>
          {game.title}
          </Link>
        </Media>
        <Container className="p-0">
          <Row>
            <Col md="10"><p className="font-italic game-creator">{game.creator}</p></Col>
            <Col md="2"><span className="fa fa-star fa-lg game-stars"/>{game.stars}</Col>
          </Row>
        </Container>      
      </Media>
    </Media>
  );
}