import React, { Component } from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import _ from 'lodash';
import './PaginatorComponent.css';

export default class Paginator extends Component {
  constructor(props) {
    super(props);

    this.prev = this.prev.bind(this);
    this.next = this.next.bind(this);
    this.toPage = this.toPage.bind(this);
  }

  render() {
    if (!this.props.numberOfPages) {
      return (
        <div></div>
      );
    }

    const items = _.range(this.props.numberOfPages)
      .map(i => {
        return (
          <PaginationItem key={i} active={this.props.page === i}>
            <PaginationLink onClick={() => this.toPage(i)} type="button">
              {i+1}
            </PaginationLink>
          </PaginationItem>
        );
      });
    return (
      <Pagination listClassName={this.props.centered ? 'paginator-centered' : null}>
        <PaginationItem disabled={this.props.page === 0}>
          <PaginationLink onClick={this.prev} type="button">
            <span aria-hidden="true">&laquo;</span>
            <span className="sr-only">Previous</span>
          </PaginationLink>
        </PaginationItem>
        {items}
        <PaginationItem disabled={this.props.page === this.props.numberOfPages - 1}>
          <PaginationLink onClick={this.next} type="button">
            <span aria-hidden="true">&raquo;</span>
            <span className="sr-only">Next</span>
          </PaginationLink>
        </PaginationItem>
      </Pagination>
    );
  }

  prev() {    
    this.toPage(this.props.page - 1);
  }

  next() {
    this.toPage(this.props.page + 1);
  }

  toPage(i) {
    if (i < 0 || i >= this.props.numberOfPages) {
      throw new Error(`Cannot move to page ${i}. Out of range 0..${this.props.numberOfPages - 1}`);
    }
    this.props.setPage(i);
  }
}