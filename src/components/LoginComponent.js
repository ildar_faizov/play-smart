import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Button, 
    Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { LocalForm, Control } from 'react-redux-form';

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoginModalShown: false
        };
        this.toggleLoginModal = this.toggleLoginModal.bind(this);
        this.login = this.login.bind(this);
    }

    componentDidUpdate() {
        if (this.props.account.account && this.state.isLoginModalShown) {
            this.setState({
                isLoginModalShown: false
            });
        }
    }

    render() {
        const pending = this.props.account.pending;
        let result;
        if (this.props.account.error) {
            result = <p className="text-danger">{this.props.account.error}</p>;
        } else if (this.props.account.account) {
            result = <p className="text-success">Login successfull!</p>;
        } else {
            result = <React.Fragment />;
        }
        return (
            <React.Fragment>
                <AccountButton 
                    account={this.props.account} 
                    logout={this.props.logout} 
                    showLogin={this.toggleLoginModal}/>
                <Modal isOpen={this.state.isLoginModalShown} toggle={this.toggleLoginModal} >
                    <ModalHeader toggle={this.toggleLoginModal}>
                        Log In
                    </ModalHeader>
                    <ModalBody>
                        <LocalForm onSubmit={this.login}>
                            <fieldset disabled={pending}>
                                <FormGroup>
                                    <Label for="login.email">Your E-Mail</Label>
                                    <Control.text model=".email" type="email" id="login.email" name="email"
                                        className="form-control" placeholder="john.smith@gmail.com" />
                                </FormGroup>
                                <FormGroup>
                                    <Label for="login.pwd">Password</Label>
                                    <Control.text model=".pwd" type="password" id="login.pwd" name="pwd"
                                        className="form-control" />
                                </FormGroup>
                                <Button type="submit" color="primary">
                                    Log In
                                    <span className={`fa fa-spinner fa-pulse ${!pending ? 'd-none' : ''}`} />
                                </Button>
                                {result}
                            </fieldset>
                        </LocalForm>
                    </ModalBody>
                </Modal>
            </React.Fragment>
        );
    }

    toggleLoginModal() {
        this.setState({
            isLoginModalShown: !this.state.isLoginModalShown
        });
    }

    login(values) {
        this.props.tryLogin(values);
    }
}

class AccountButton extends Component {
    constructor(props) {
        super(props);

        this.state = {
            logoutDropdownOpen: false
        };
        this.toggleLogoutDropdown = this.toggleLogoutDropdown.bind(this);
        this.logout = this.logout.bind(this);
    }

    render() {
        if (!this.props.account.account) {
            return (
                <Button outline onClick={this.props.showLogin} disabled={this.props.account.pending}>
                    <span className="fa fa-sign-in fa-lg" /> Log In
                </Button>
            );
        } else {
            return (
                <Dropdown isOpen={this.state.logoutDropdownOpen} toggle={this.toggleLogoutDropdown}>
                    <DropdownToggle caret>
                        {this.props.account.account.email}
                    </DropdownToggle>
                    <DropdownMenu>
                        <DropdownItem onClick={this.logout}>Logout</DropdownItem>
                    </DropdownMenu>
                </Dropdown>
            );
        }
    }

    toggleLogoutDropdown() {
        this.setState({
            logoutDropdownOpen: !this.state.logoutDropdownOpen
        });
    }

    logout() {
        console.log('Logout called');
        this.toggleLogoutDropdown();
        this.props.logout();
    }
}