import React, { Component } from 'react';
import { Modal, ModalHeader, ModalBody, FormGroup, Label, Button } from 'reactstrap';
import { LocalForm, Control, Errors } from 'react-redux-form';
import juration from 'juration';

export class Timer extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isTimerModalOpen: false,
            remains: null // value in seconds
        };

        this.setTimer = this.setTimer.bind(this);
        this.toggleTimerModal = this.toggleTimerModal.bind(this);
        this.tick = this.tick.bind(this);

        this.timerId = null;
    }

    componentDidUpdate() {
        if (this.props.timer.startTime && this.props.timer.durationInSeconds) {
            if (!this.timerId) {
                this.startTick();
            } else if (this.remains <= 0) {
                this.stopTick();
            }
        } else {
            this.stopTick();
            this.setState((prevState) => {
                if (prevState.remains != null) {
                    return {
                        remains: null
                    }
                }
            });
        }
    }

    componentWillUnmount() {
        this.stopTick();
    }

    render() {
        let timerColor;
        let timerText;
        let outline;
        if (this.state.remains > 0) {
            timerColor = 'info';
            timerText = (
                <React.Fragment>
                    {juration.stringify(this.state.remains, {format: 'micro', units: 3})}
                </React.Fragment>
            );
        } else if (this.state.remains === 0) {
            timerColor = 'danger';
            timerText = (
                <React.Fragment>
                    <span className="fa fa-times-circle fa-lg" /> Time over
                </React.Fragment>
            );
        } else {
            timerColor = 'primary';
            timerText = (
                <React.Fragment>
                    <span className="fa fa-clock-o fa-lg" /> Timer
                </React.Fragment>
            );
            outline = true;        
        }
        return (
            <React.Fragment>
                <Button outline={outline} color={timerColor} className="mr-3" onClick={this.toggleTimerModal}>
                    {timerText}
                </Button>
                <SetTimerModal 
                    isOpen={this.state.isTimerModalOpen} 
                    setTimer={this.setTimer}
                    close={this.toggleTimerModal}/>
            </React.Fragment>
        );
    }

    setTimer(value) {
        this.toggleTimerModal();
        this.props.setTimer(value);
    }

    toggleTimerModal() {
        this.setState({
            isTimerModalOpen: !this.state.isTimerModalOpen
        });
    }

    startTick() {
        this.stopTick();
        this.tick();
        this.timerId = setInterval(this.tick, 500);
    }

    stopTick() {
        if (this.timerId != null) {
            clearInterval(this.timerId);
            this.timerId = null;
        }
    }

    tick() {
        this.setState((_prevState, props) => {
            if (props.timer.startTime && props.timer.durationInSeconds) {
                const remains = Math.max(Math.ceil((props.timer.startTime - new Date().getTime()) / 1000) 
                    + props.timer.durationInSeconds, 0);
                return { remains };
            } else {
                return { remains: null };
            }
        });
    }
}

const required = (val) => !!val;
const correctDuration = (val) => {
    if (val) {
        try {
            juration.parse(val);
            return true;
        } catch (error) {}
    }
    return false;
}

function SetTimerModal(props) {
    const handleSubmit = (values) => {
        const durationInSecs = juration.parse(values.value);
        props.setTimer(durationInSecs);
    }

    return (
        <Modal isOpen={props.isOpen} toggle={() => props.close()}>
            <ModalHeader toggle={() => props.close()}>
                Timer Value
            </ModalHeader>
            <ModalBody>
                <LocalForm onSubmit={handleSubmit}>
                    <FormGroup>
                        <Label htmlFor="timer.value"/>
                        <Control.text model=".value" 
                            id="timer.value" 
                            className="form-control" 
                            placeholder="1h 30m"
                            validators={{
                                required,
                                correctDuration
                            }}/>
                        <Errors 
                            model=".value" 
                            className="text-danger"
                            show="touched" 
                            messages={{
                                required: 'Required',
                                correctDuration: 'Must represent correct duration, e.g. "1h 30m", "5min", "2hour"'
                            }}/>
                    </FormGroup> 
                    <Button color="primary">
                        Submit
                    </Button>
                    <Button color="warning" className="ml-2" onClick={() => props.setTimer()}>
                        Reset
                    </Button>
                    <Button color="dark" outline className="ml-2" onClick={() => props.close()}>
                        Cancel
                    </Button>
                </LocalForm>
            </ModalBody>
        </Modal>
    );
}