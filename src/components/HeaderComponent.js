import React, { Component } from 'react';
import { Navbar, Nav, NavItem, NavbarToggler, Collapse, Container } from 'reactstrap';
import { NavLink } from 'react-router-dom';
import Login from './LoginComponent';
import { Timer } from './TimerComponent';

export default class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isNavOpen: true
    }

    this.toggleNav = this.toggleNav.bind(this);
  }

  render() {
    return (
      <div className="container mb-3">
        <Navbar expand="md" className="bg-light">
          <Container>
            <NavbarToggler onClick={this.toggleNav} />
            <Collapse isOpen={this.state.isNavOpen} navbar>
              <Nav navbar>
                <NavItem>
                  <NavLink className="nav-link" to="/home" >
                    <span className="fa fa-home fa-lg" /> Home
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="nav-link" to="/contribute">
                    <span className="fa fa-cloud-upload fa-lg" /> Contribute
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="nav-link" to="/parents">
                    <span className="fa fa-users fa-lg" /> Parents
                  </NavLink>
                </NavItem>
                <NavItem>
                  <NavLink className="nav-link" to="/contacts">
                    <span className="fa fa-address-card fa-lg" /> Contact Us
                    </NavLink>
                </NavItem>
              </Nav>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <Timer 
                    timer={this.props.timer} 
                    setTimer={this.props.setTimer}/>
                </NavItem>
                <NavItem>
                  <Login                
                    tryLogin={this.props.tryLogin}
                    account={this.props.account} 
                    logout={this.props.logout}/>
                </NavItem>
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </div>
    );
  }

  toggleNav() {
    this.setState({
      isNavOpen: !this.state.isNavOpen
    });
  }
}