const required = (val) => !!val;

const boundedInt = ({min, max} = {}) => (val) => {
  try {
    const num = parseInt(val, 10);
    return (min == null || num >= min) 
      && (max == null || num <= max);
  } catch (e) {
    return false;
  }
};

const byRegExp = (regexp) => (val) => regexp.test(val);

const boundedString = ({minLength, maxLength} = {}) => (val) => 
  val && val.length 
    && (minLength == null || val.length >= minLength) 
    && (maxLength == null || val.length <= maxLength)

export default {
  required,
  boundedInt,
  byRegExp,
  boundedString,
  isInt: boundedInt()
};