import fetch from 'cross-fetch';
import { BASE_URL } from './baseUrl';

/* Utility functions for fetch */

export function post(domain, object) {
    const params = {
        method: 'POST',
        credentials: 'same-origin',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(object)
    };
    return fetch(BASE_URL + domain, params)
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                let error = new Error(`Error ${response.status}: ${response.statusText}`);
                error.response = response;
                throw error;
            }
        }, error => {
            throw new Error(error.message);
        })
        .then(response => response.json());
}

export function get(domain, params) {
    let queryString = BASE_URL + domain;
    if (params) {
        queryString += '?' + Object.keys(params)
            .map(key => encodeURIComponent(key) + '=' + encodeURIComponent(params[key]))
            .join('&');
    }
    return fetch(queryString)
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                let error = new Error(`Error ${response.status}: ${response.statusText}`);
                error.response = response;
                throw error;
            }
        }, error => {
            throw new Error(error.message);
        })
        .then(response => response.json())
        .catch(error => {
            console.log(error);
            throw error;
        });
};