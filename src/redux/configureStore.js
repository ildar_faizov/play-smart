import { createStore, combineReducers, applyMiddleware } from 'redux';
import { createForms } from 'react-redux-form';
import thunk from 'redux-thunk';
import logger from 'redux-logger';
import { Areas } from './areas';
import { Games } from './games';
import { Game } from './game';
import { InitialFilterState } from './filterForm';
import { Account } from './account';
import { Timer } from './timer';
import { Contribute } from './contribute';

export const ConfigureStore = () => {
  const reducers = {
    areas: Areas,
    games: Games,
    game: Game,
    account: Account,
    timer: Timer,
    gameUpload: Contribute,
    ...createForms({
      filter: InitialFilterState
    })
  }
  return createStore(combineReducers(reducers), applyMiddleware(thunk, logger));
}