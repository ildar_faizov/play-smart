import * as ActionTypes from './ActionTypes';

const initialState = {
  isLoading: false,
  error: null,
  pageLength: 5,
  currentPage: 0,
  total: 0,
  games: []
};

export const Games = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.APPLY_FILTER:
      return {
        ...state,
        isLoading: true,
        error: null,
        pageLength: 5,
        currentPage: action.payload.currentPage,
        total: 0,
        games: []
      };
    case ActionTypes.FILTER_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        total: 0,
        games: []
      };
    case ActionTypes.FILTER_FETCHED:
      return {
        ...state,
        isLoading: false,
        error: null,
        total: action.payload.total,
        games: action.payload.games
      };
    default:
      return state;
  }
};