import * as ActionTypes from './ActionTypes';

const initialState = {
  isLoading: false,
  error: null,
  areas: null
};

export const Areas = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.AREAS_FETCHING:
      return {
        ...state,
        isLoading: true,
        error: null,
        areas: null
      };
    case ActionTypes.AREAS_FETCHED:
      return {
        ...state,
        isLoading: false,
        error: null,
        areas: action.payload
      };
    case ActionTypes.AREAS_FAILED:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        areas: null
      };
    default:
      return state;
  }
};