import * as ActionTypes from './ActionTypes';
import { BASE_URL } from '../shared/baseUrl';
import { get, post } from '../shared/fetchUtils';
import _ from 'lodash';
import loadScript from 'simple-load-script';
import { v4 } from 'uuid';

export const fetchAreas = () => (dispatch, getState) => {
  const areasState = getState().areas;
  if (areasState.isLoading || areasState.areas) {
    return; // no need to reload areas
  }

  dispatch({
    type: ActionTypes.AREAS_FETCHING
  });

  return get('areas')
    .then(areas => {
      areas.sort(); // in-place
      dispatch({
        type: ActionTypes.AREAS_FETCHED,
        payload: areas
      });
    })
    .catch(error => dispatch({
      type: ActionTypes.AREAS_FAILED,
      payload: error.message
    }));
}

export const fetchGames = (filter, pagination) => dispatch => {
  dispatch({
    type: ActionTypes.APPLY_FILTER,
    payload: {
      currentPage: pagination.currentPage
    }
  });

  const queryParams = {
    '_sort': 'stars',
    '_order': 'desc'
  };

  const filterByTitle = game => 
    !filter.title || game.title.toLowerCase().indexOf(filter.title.toLowerCase()) >= 0;

  const filterByAge = game => 
    game.age.min <= filter.age.max && game.age.max >= filter.age.min;
  
  const filterByArea = game => 
    _(game.areas).some(item => filter.areas.indexOf(item) >= 0);

  const filterBy = game => 
    filterByAge(game) && filterByArea(game) && filterByTitle(game);

  const paginate = games => {
    const start = pagination.pageLength * pagination.currentPage;
    return {
      total: games.length,
      games: games.slice(start, start + pagination.pageLength)
    };
  }

  return get('games', queryParams)
    .then(games => games.filter(game => filterBy(game)))
    .then(paginate)
    .then(data => dispatch({
      type: ActionTypes.FILTER_FETCHED,
      payload: data
    }))
    .catch(error => dispatch({
      type: ActionTypes.FILTER_FAILED,
      payload: error.message
    }))
}

export const fetchGame = id => dispatch => {
  dispatch({
    type: ActionTypes.FETCH_GAME,
    payload: id
  });

  get(`games/${id}`)
    .then(game => Promise.all([game, loadScript(BASE_URL + 'sourcecode/' + game.source)]))
    .then(([game]) => {
      const gameFunction = window['GameImpl'];
      game.instance = Object.create(gameFunction.prototype);
      gameFunction.call(game.instance);
      dispatch({
        type: ActionTypes.GAME_FETCHED,
        payload: game
      });
    })
    .catch(error => {
      console.error(error);
      throw error;
    })
    .catch(error => dispatch({
      type: ActionTypes.GAME_FAILED,
      payload: error
    }));
}

export const tryLogin = (credentials) => dispatch => {
  dispatch({
    type: ActionTypes.TRY_LOGIN,
    payload: credentials
  });

  // TODO: implemented on client for the sake of the back-end
  get('accounts')
    .then(accounts => _.find(accounts, ['email', credentials.email]))
    .then(account => {
      if (!account) {
        throw new Error(`Account for "${credentials.email}" not found`);
      } 
      if (account.pwd !== credentials.pwd) {
        throw new Error(`Incorrect password for "${credentials.email}"`);
      }
      dispatch({
        type: ActionTypes.LOGIN_SUCCESSFULL,
        payload: account
      });
    })
    .catch(error => dispatch({
      type: ActionTypes.LOGIN_FAILED,
      payload: error.message
    }));
}

export const logout = () => dispatch => (dispatch({
  type: ActionTypes.LOGOUT
}));

export const setTimer = duration => dispatch => (dispatch({
  type: duration ? ActionTypes.SET_TIMER : ActionTypes.CANCEL_TIMER,
  payload: duration
}));

export const uploadGame = (game) => (dispatch) => {
  const requestId = v4();
  dispatch({
    type: ActionTypes.UPLOADING_GAME,
    payload: {
      game, 
      requestId
    }
  });

  post('games', game)
    .then(response => {
      console.log('Game saved', response);
      dispatch({
        type: ActionTypes.UPLOADING_GAME_SUCCEEDED,
        payload: {
          response,
          requestId
        }
      });
    })
    .catch(error => {
      dispatch({
        type: ActionTypes.UPLOADING_GAME_FAILED,
        payload: {
          error: error.message,
          requestId
        }
      });
    })
}