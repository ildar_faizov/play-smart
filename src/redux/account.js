import * as ActionTypes from './ActionTypes';

const initialState = {
    pending: false,
    account: null,
    error: null
}

export const Account = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.TRY_LOGIN:
            return {
                ...state,
                pending: true,
                account: null,
                error: null
            };
        case ActionTypes.LOGIN_FAILED:
            return {
                ...state,
                pending: false,
                account: null,
                error: action.payload
            };
        case ActionTypes.LOGIN_SUCCESSFULL: 
            return {
                ...state,
                pending: false,
                account: action.payload,
                error: null
            };
        case ActionTypes.LOGOUT:
            return {
                ...state,
                pending: false,
                account: null,
                error: null
            };
        default:
            return state;
    }
}