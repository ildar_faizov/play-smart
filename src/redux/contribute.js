import * as ActionTypes from './ActionTypes';

const initialState = {
  requestId: null,
  isPending: false,
  isSuccessful: false,
  error: null
};

export const Contribute = (state = initialState, action) => {
  switch (action.type) {
    case ActionTypes.UPLOADING_GAME:
      return {
        ...state,
        requestId: action.payload.requestId,
        isPending: true,
        isSuccessful: false,
        error: null
      };
    case ActionTypes.UPLOADING_GAME_SUCCEEDED:
      return {
        ...state,
        requestId: action.payload.requestId,
        isPending: false,
        isSuccessful: true,
        error: null
      };
    case ActionTypes.UPLOADING_GAME_FAILED: 
      return {
        ...state,
        requestId: action.payload.requestId,
        isPending: false,
        isSuccessful: false,
        error: action.payload.error
      };
    default:
      return state;
  }
}