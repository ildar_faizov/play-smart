import * as ActionTypes from './ActionTypes';

const initialState = {
    startTime: null,
    durationInSeconds: null
};

export const Timer = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.SET_TIMER:
            return {
                ...state,
                startTime: new Date(),
                durationInSeconds: action.payload
            };
        case ActionTypes.CANCEL_TIMER:
            return {
                ...state,
                ...initialState
            };
        default:
            return state;
    }
};