import * as ActionTypes from './ActionTypes';

const initialState = {
    isLoading: true,
    error: null,
    gameId: null,
    game: null
};

export const Game = (state = initialState, action) => {
    switch (action.type) {
        case ActionTypes.FETCH_GAME:
            return {
                ...state,
                ...initialState,
                gameId: action.payload
            };
        case ActionTypes.GAME_FAILED: 
            return {
                isLoading: false,
                error: action.payload.message,
                gameId: state.gameId,
                game: null
            };
        case ActionTypes.GAME_FETCHED: 
            return {
                isLoading: false,
                error: null,
                gameId: state.gameId,
                game: action.payload
            }
        default:
            return state;
    }
}