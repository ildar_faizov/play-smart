export const MIN_AGE = 4;
export const MAX_AGE = 12;

export const InitialFilterState = {
  age: {
    min: MIN_AGE,
    max: MAX_AGE
  },
  title: '',
  // selected areas go in the form of 'area-${id}'
  showVisited: false
};