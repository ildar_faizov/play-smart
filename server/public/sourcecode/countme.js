function GameImpl() {
  console.log(`Game created at ${new Date()}`);
}

GameImpl.prototype.init = function(canvas, api) {
  console.log(`Game initialized at ${new Date()}`, canvas, api);

  const ctx = canvas.getContext('2d');
  ctx.font = '16pt serif';
  ctx.fillText(`Welcome to ${api.game.title}`, 10, 100);
}

GameImpl.prototype.destroy = function() {
  console.log(`Game destroyed at ${new Date()}`);
}